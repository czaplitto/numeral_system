# NumSystem

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Description

Provide correct information about source numeral system, target numeral system and number. Then click the button save to count the result. You can provide any negative numbers, fractions, etc. It is not possible to provide numbers or letters that are not used in the numeral system that was chosen as the initial one. You can choose ANY numeral system that is higher than 2. I avoided using ParseInt and toString methods, because then you have to choose the numeral system from 2 to 36 and the result will be in most cases rounded to the integer. However, I left a comment in the code that shows how to use these methods.
