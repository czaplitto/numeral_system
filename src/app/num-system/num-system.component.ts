import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-num-system',
  templateUrl: './num-system.component.html',
  styleUrls: ['./num-system.component.css']
})
export class NumSystemComponent implements OnInit {
  alphabet: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  show: string;
  systemForm: FormGroup;
  isSubmitted: boolean = false;
  see: number;

  constructor(
  ) {
    this.systemForm = new FormGroup({
      'source': new FormControl(null, [Validators.required, Validators.pattern('^[2-9]$|^[1-9][0-9]+$')]),
      'aim': new FormControl(null, [Validators.required, Validators.pattern('^[2-9]$|^[1-9][0-9]+$')]),
      'number': new FormControl(null, [Validators.required]),
    });
  }

  ngOnInit(): void {
  }

  onKey(e: any): void {
    this.see = e.target.value;
    if (e.target.value === "2") {
      this.systemForm.controls["number"].setValidators([Validators.required, Validators.pattern('[0-1]+(\.[0-1]+)?')])
    } else {
      let fromAlpha = this.alphabet.slice(0, this.see > 36 ? 36 : this.see - 10);
      let i = !this.see ? 1 : this.see >= 10 ? 9 : this.see - 1 || 1;
      let choose = this.see <= 10 ? `^\-?[1-${i}]$|^\-?[1-${i}][0-${i}]+$|^\-?[0-${i}]+\.[0-${i}]+$` : `^\-?[1-9${fromAlpha}${fromAlpha.toLowerCase()}]$|^\-?[1-9${fromAlpha}${fromAlpha.toLowerCase()}][0-9${fromAlpha}${fromAlpha.toLowerCase()}]+$|^\-?[0-9${fromAlpha}${fromAlpha.toLowerCase()}]+\.[0-9${fromAlpha}${fromAlpha.toLowerCase()}]+$`
      this.systemForm.controls["number"].setValidators([Validators.required, Validators.pattern(choose)]);
    }
    this.systemForm.controls["number"].updateValueAndValidity()
  }

  submit() {
    this.isSubmitted = true;
    let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let finalSum = '';
    let arr = [];
    let collect = [];
    let plusMinus = this.systemForm.value.number[0] === '-';
    let firstSys = this.systemForm.value.source;
    let secondSys = this.systemForm.value.aim;
    let num = this.systemForm.value.number.includes('-') ? this.systemForm.value.number.replace('-', '') : this.systemForm.value.number;

    let checkLetter = (example) => [...example.toUpperCase()].map(x => alphabet.indexOf(x) !== -1 ? alphabet.indexOf(x) + 10 : x);

    function toDecimal(numToDeci: any) {
      let [beforeDot, afterDot] = numToDeci.split(".");
      let sumAfterDot = 0;
      let minusResult = 0;
      let sum = 0;
      if (beforeDot === numToDeci) {
        beforeDot = checkLetter(numToDeci);
      } else {
        beforeDot = checkLetter(beforeDot);
        afterDot = checkLetter(afterDot);
        for (let i = 0; i < afterDot.length; i++) {
          minusResult = parseFloat((minusResult + afterDot[i] * (firstSys ** (-1 - i))).toFixed(20));
        }
        sumAfterDot = [...`${minusResult}`].slice(0, 10).every(x => +x === secondSys - 1) ? 1 : minusResult;
      }
      for (let i = 0; i < beforeDot.length; i++) {
        sum += parseFloat((beforeDot[i] * (firstSys ** (beforeDot.length - i - 1))).toFixed(20));
      }
      return typeof afterDot === 'undefined' ? `${sum}` : `${sum + sumAfterDot}`;
    }

    function fromDecimal(numFromDeci: any) {
      if (numFromDeci === 0) return arr;
      arr.unshift(numFromDeci % secondSys);
      return fromDecimal(Math.floor(numFromDeci / secondSys));
    }

    function numToLetter(example) {
      let str = "";
      for (let i = 0; i < example.length; i++) {
        if (example[i] >= 10 && example[i] < secondSys && example[i] <= 35) {
          str += alphabet[example[i] - 10];
        } else {
          str += example[i];
        }
      }
      return str;
    }

    function extractNumbers(sum, long) {
      let finalArr = [];
      let count = 0;
      for (let i = 0; i < long.length; i++) {
        finalArr.push(sum.substr(count, long[i]));
        count += long[i];
      }
      return finalArr;
    }

    function fractionFromDeci(fracFromDeci, exp = 0) {
      if (fracFromDeci === 0 || collect.length === 35) return collect;
      let count = parseFloat((+`0.${fracFromDeci}` * +secondSys).toFixed(20));
      let [before, after = 0] = `${count}`.split(".");
      collect.push(before);
      return fractionFromDeci(after, exp + 1);
    }

    function mainFromDecimal(a) {
      let [beforeDot, afterDot] = a.split(".");
      let last = '';
      let arrWithoutZero = [];
      if (beforeDot === a) {
        finalSum = fromDecimal(beforeDot);
        if (secondSys < 11) {
          finalSum = [...finalSum].join("");
        } else {
          last = finalSum[finalSum.length - 1];
          arrWithoutZero = [...finalSum].filter(x => x);
          finalSum = numToLetter(finalSum);
        }
      } else {
        let sumBeforeDot = fromDecimal(beforeDot);
        fractionFromDeci(afterDot);
        finalSum = `${+sumBeforeDot.join("") + +(collect.slice(0, 10).every(x => +x === secondSys - 1) ? 1 : '0.' + collect.join(""))}`;
        if (secondSys < 11) {
          return finalSum;
        } else {
          last = collect[collect.length - 1];
          arrWithoutZero = collect.filter(x => +x);
          let follow = sumBeforeDot.map(x => `${x}`.length);
          let follow2 = collect.map(x => `${x}`.length);
          let [firstPart, secondPart] = finalSum.split(".");
          let one = extractNumbers(firstPart, follow);
          let two = extractNumbers(secondPart, follow2) || 0;
          finalSum = numToLetter(one) + '.' + numToLetter(two);
        }
      }
      if (arrWithoutZero[arrWithoutZero.length - 1] === "10" || arrWithoutZero[arrWithoutZero.length - 1] === "20" || (last === "0" && arrWithoutZero[arrWithoutZero.length - 1] === "1" || last === "0" && arrWithoutZero[arrWithoutZero.length - 1] === "2")) {
        if (finalSum[finalSum.length - 1] === "1") {
          finalSum = finalSum.slice(0, -1) + "A";
        } else if (finalSum[finalSum.length - 1] === "2") {
          finalSum = finalSum.slice(0, -1) + "B";
        }
      }
      return finalSum;
    }

    if (firstSys === "10") {
      mainFromDecimal(num);
    } else if (secondSys === "10") {
      finalSum = toDecimal(num);
    } else {
      let anotherNum = toDecimal(num);
      mainFromDecimal(anotherNum);
      //todo You can use ParseInt and toString methods, but then you have to choose the numeral system from 2 to 36 and
      // the result will be in most cases rounded to the integer. Below you can find the example of this how you can imply this methods here:
      /*
      let a = parseInt(num, first);
      finalSum = a.toString(second);
      */
    }
    finalSum = !!plusMinus ? `-${finalSum}` : finalSum;
    this.show = `The result is ${finalSum}`;
  }
}
