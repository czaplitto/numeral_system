import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumSystemComponent } from './num-system.component';

describe('NumSystemComponent', () => {
  let component: NumSystemComponent;
  let fixture: ComponentFixture<NumSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NumSystemComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
